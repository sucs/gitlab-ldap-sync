# Gitlab LDAP Sync thing


## Upstream project
https://github.com/imranh2/gitlab-ldap-sync


## Deployment
Deployed on to the projectsvm/gitlab host, installed as a cron job for root.


```
*/15 * * * * cd /root/gitlab-ldap-sync && git reset --hard HEAD; git pull && GITLAB_TOKEN=[REDACTED] python sync.py > gitlab-ldap-sync.log
```


Uses my (imranh) gitlab key, so if my user is disabled or has admin privs pulled then it'll stop working.
